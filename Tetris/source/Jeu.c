﻿#include "Jeu.h"

Grille initGrille() {
	return getNewGrille();
}

void afficherGrille(Grille grille, SDL_Rect terrain, SDL_Surface* win_surf) {	
	for (int i = 0; i < GRILLE_COLUMN_NUMBER; i++) {
		for (int j = 0; j < GRILLE_LINE_NUMBER; j++) {
			int red = 0;
			int green = 0;
			int blue = 0;
			switch (grille.s_Tetris[i][j]) {
			case CARRE_BLEU:
				blue = 255;
				break;
			case LIGNE_ROUGE:
				red = 255;
				break;
			case TRIANGLE_VERT:
				green = 255;
				break;
			case ZIG1_CYAN:
				green = 255;
				blue = 255;
				break;
			case ZIG2_GRIS:
				red = 169;
				green = 169;
				blue = 169;
				break;
			case L1_ROSE:
				red = 255;
				green = 105;
				blue = 180;
				break;
			case L2_JAUNE:
				red = 255;
				green = 255;
				break;
			case TETRIS_BLOCKING:
				red = 100;
				green = 100;
				blue = 100;
				break;
			default:
				break;
			};
			SDL_Rect dstPiece = { i * TAILLE_BLOC + terrain.x, j * TAILLE_BLOC + terrain.y, TAILLE_BLOC, TAILLE_BLOC };
			SDL_FillRect(win_surf, &dstPiece, SDL_MapRGB(win_surf->format, red, green, blue));
		}
	}	
}

void drawSeparationLines(SDL_Rect terrain, SDL_Surface* win_surf) {
	int terrainW = GRILLE_COLUMN_NUMBER * TAILLE_BLOC;
	int terrainH = GRILLE_LINE_NUMBER * TAILLE_BLOC;
	// display lines to separate blocks
	for (int i = 0; i < GRILLE_LINE_NUMBER; i++) {
		SDL_Rect line = { terrain.x,  terrain.y + TAILLE_BLOC * (i + 1), terrainW, 1 };
		SDL_FillRect(win_surf, &line, SDL_MapRGB(win_surf->format, 0, 0, 0));
	}
	for (int i = 0; i < GRILLE_COLUMN_NUMBER; i++) {
		SDL_Rect line = { terrain.x + TAILLE_BLOC * (i + 1),  terrain.y, 1, terrainH };
		SDL_FillRect(win_surf, &line, SDL_MapRGB(win_surf->format, 0, 0, 0));
	}
}
//renvoie 0 si le mouvement est possible, 1 sinon
int deplacerPiece(Piece piece, int newX, int newY, Grille grille) {
	Piece newPiece = new_piece(newX, newY, piece.type);

	if (pieceEstPlacableGrille(newPiece, grille)) {
		return 1;
	}
	else {
		return 0;
	}
}


Piece deplacementAutoPiece(Piece piece, Grille* grille, int* pieceEnCoursDePositionnement) {
	if (deplacerPiece(piece, piece.x, piece.y + 1, *grille) == 1) {
		piece.y = piece.y + 1;
	}
	else {
		*pieceEnCoursDePositionnement = 0;
		*grille = addPieceToGrille(piece, *grille);
	}
	return piece;
}

//retourne 1 si pla�able, 0 sinon
int pieceEstPlacableGrille(Piece piece, Grille grille) {
	int res = 1;
	int x = piece.x;
	int y = piece.y;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 2; j++) {
			if (piece.forme[i][j][0] != VIDE && piece.forme[i][j][1] != VIDE && piece.forme[i][j][0] != VIDEROTATED && piece.forme[i][j][1] != VIDEROTATED) {
				int xGrille = x + piece.forme[i][j][0];
				int yGrille = y + piece.forme[i][j][1];

				if (xGrille >= GRILLE_COLUMN_NUMBER || yGrille >= GRILLE_LINE_NUMBER) {
					res = 0;
				}
				if (grille.s_Tetris[xGrille][yGrille] != TETRIS_RIEN) {
					res = 0;
				}
			}
		}
	}
	return res;
}

Grille addPieceToGrille(Piece piece, Grille grille) {
	int x = piece.x;
	int y = piece.y;
	if (pieceEstPlacableGrille(piece, grille) == 1) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				if (piece.forme[i][j][0] != VIDE && piece.forme[i][j][1] != VIDE && piece.forme[i][j][0] != VIDEROTATED && piece.forme[i][j][1] != VIDEROTATED) {
					int xGrille = x + piece.forme[i][j][0];
					int yGrille = y + piece.forme[i][j][1];
					int color = 0;
					switch (piece.type) {
					case BARRE:
						color = LIGNE_ROUGE;
						break;
					case BLOC:
						color = CARRE_BLEU;
						break;
					case TE:
						color = TRIANGLE_VERT;
						break;
					case LAMBDA:
						color = L1_ROSE;
						break;
					case GAMMA:
						color = L2_JAUNE;
						break;
					case BIAIS:
						color = ZIG1_CYAN;
						break;
					case BIAIS_INVERSE:
						color = ZIG2_GRIS;
						break;
					}
					grille.s_Tetris[xGrille][yGrille] = color;
				}
			}
		}
	}
	else {
		printf("%s\n", "la piece n'est pas positionnable");
	}
	//showGrille(grille);
	return grille;
}

void afficherPieceEnCours(Piece piece, int terrainX, int terrainY, SDL_Surface* win_surf) {
	int red = 0;
	int green = 0;
	int blue = 0;
	switch (piece.type) {
	case BARRE:
		red = 255;
		break;
	case BLOC:
		blue = 255;
		break;
	case TE:
		green = 255;
		break;
	case LAMBDA:
		red = 255;
		green = 105;
		blue = 180;
		break;
	case GAMMA:
		red = 255;
		green = 255;
		break;
	case BIAIS:
		green = 255;
		blue = 255;
		break;
	case BIAIS_INVERSE:
		red = 169;
		green = 169;
		blue = 169;
		break;
	}
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 2; j++) {
			int xBloc = piece.x + piece.forme[i][j][0];
			int yBloc = piece.y + piece.forme[i][j][1];
			SDL_Rect dstPiece = { xBloc * TAILLE_BLOC + terrainX, yBloc * TAILLE_BLOC + terrainY, TAILLE_BLOC, TAILLE_BLOC };
			SDL_FillRect(win_surf, &dstPiece, SDL_MapRGB(win_surf->format, red, green, blue));
		}
	}
}

//retourne le nombre de lignes completes et les vides
int ligneComplete(Grille* grille) {
	int lines = 0;
	Grille tmpGrille = *grille;
	for (int i = 0; i < GRILLE_LINE_NUMBER; i++) {
		int complete = 1;
		for (int j = 0; j < GRILLE_COLUMN_NUMBER; j++) {
			if (tmpGrille.s_Tetris[j][i] == TETRIS_RIEN) {
				complete = 0;
			}
		}
		if (complete == 1) {
			lines++;
			for (int j = 0; j < GRILLE_COLUMN_NUMBER; j++) {
				tmpGrille.s_Tetris[j][i] = TETRIS_RIEN;
			}
			for (int y = 1; y < i; y++) {
				for (int j = 0; j < GRILLE_COLUMN_NUMBER; j++) {
					tmpGrille.s_Tetris[j][y] = tmpGrille.s_Tetris[j][y-1];
					//tmpGrille.s_Tetris[j][y-1] = TETRIS_RIEN;
				}
			}
		}
	}
	*grille = tmpGrille;
	return lines;
}

// original nintendo scoring system
// https://tetris.fandom.com/wiki/Scoring
int scoreByLines(int linesMultiplier) {
	int score = 0;
	switch (linesMultiplier)
	{
	case 1:
		score = 40;
		break;
	case 2:
		score = 100;
		break;
	case 3:
		score = 300;
		break;
	case 4:
		score = 1200;
		break;
	default:
		break;
	}
	return score;
}

void updateGameLevel(int* gameLevel, int gameLinesScored) {
	*gameLevel = (int)floor(gameLinesScored / 10);
}

void updateScore(int linesMultiplier, int gameLevel, int* gameLinesScored, int* gameScore) {

	*gameLinesScored += linesMultiplier;
	int baseScore = scoreByLines(linesMultiplier);
	*gameScore += baseScore * (gameLevel + 1);
}

void putBlockingLinesInGrille(int blockingLines, Grille* grille) {
	Grille tmpGrille = *grille;
	int blockingIndex = GRILLE_LINE_NUMBER-1;
	
	int lineIndex = blockingIndex - getNumberBlockedLines(*grille);
	int limitIndex = blockingIndex - blockingLines;
	for (lineIndex; lineIndex > limitIndex; lineIndex--) {
		for (int j = 0; j < GRILLE_COLUMN_NUMBER; j++) {
			tmpGrille.s_Tetris[j][lineIndex] = TETRIS_BLOCKING;
		}
	}

	*grille = tmpGrille;
}