#include "Ia.h"

void aiMovesPiece(Piece* pieceToMove, Grille grille, Piece pieceToMimic) {
	if (pieceToMove->rotation != pieceToMimic.rotation) {
		aiRotatesPiece(pieceToMove, grille);
	}
	if (pieceToMove->x < pieceToMimic.x) {
		aiMovesPieceRight(pieceToMove, grille);
	}
	if (pieceToMove->x > pieceToMimic.x) {
		aiMovesPieceLeft(pieceToMove, grille);
	}
	if (pieceToMove->y != pieceToMimic.y) {
		aiMovesPieceDown(pieceToMove, grille);
	}
}

Piece getBestPieceByScore(Piece* pieceToMove, Grille grille) {
	double score = 0;
	double bestScore = 0;
	Piece bestPiece = { 0 };
	Piece workingPiece = *pieceToMove;

	//for each rotation, check best emplacement for current piece
	for (int rotation = 0; rotation < 4; rotation++) {
		Piece piece = workingPiece;
		for (int i = 0; i < rotation; i++) {
			aiRotatesPiece(&piece, grille);
		}

		while (aiMovesPieceLeft(&piece, grille));

		while (pieceEstPlacableGrille(piece, grille)) {
			Piece pieceSet = piece;
			while (aiMovesPieceDown(&pieceSet, grille));

			Grille grid = grille;
			grid = addPieceToGrille(pieceSet, grid);

			score = HEIGHT_WEIGHT * getHeight(grid) + LINES_WEIGHT * getLines(grid) - HOLES_WEIGHT * getHoles(grid) - BUMPINESS_WEIGHT * getBumpiness(grid);

			if (score > bestScore || bestScore == 0) {
				bestScore = score;
				bestPiece = piece;
			}

			piece.x++;
		}
	}
	return bestPiece;
	
}

bool aiMovesPieceLeft(Piece* pieceToMove, Grille grille) {
	if (deplacerPiece(*pieceToMove, pieceToMove->x - 1, pieceToMove->y, grille) == 1) {
		pieceToMove->x = pieceToMove->x - 1;
		return true;
	}
	return false;
}

bool aiMovesPieceRight(Piece* pieceToMove, Grille grille) {
	if (deplacerPiece(*pieceToMove, pieceToMove->x + 1, pieceToMove->y, grille) == 1) {
		pieceToMove->x = pieceToMove->x + 1;
		return true;
	}
	return false;
}

bool aiRotatesPiece(Piece* pieceToMove, Grille grille) {
	Piece pieceRotated = rotatePiece(*pieceToMove);
	if (pieceEstPlacableGrille(pieceRotated, grille) == 1) {
		*pieceToMove = pieceRotated;
		return true;
	}
	return false;
}

bool aiMovesPieceDown(Piece* pieceToMove, Grille grille) {
	if (deplacerPiece(*pieceToMove, pieceToMove->x, pieceToMove->y + 1, grille) == 1) {
		pieceToMove->y = pieceToMove->y + 1;
		return true;
	}
	return false;
}