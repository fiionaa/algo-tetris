﻿#include "JeuMulti.h"

SDL_Rect btnBackMulti = { 225, 500, 50, 150 };
SDL_Surface* win_surf;
SDL_Window* pWindow;

SDL_Rect terrainJoueur;
SDL_Rect terrainIa;
SDL_Rect textTime;
SDL_Rect zoneNextPiece;

Grille grilleJoueur;
Grille grilleIa;
Piece pieceEnCoursJoueur;
Piece pieceEnCoursIa;
Piece nextPieceJoueur;
Piece nextPieceIa;

int pieceEnCoursDePositionnementJoueur = 0;
int pieceEnCoursDePositionnementIa = 0;

int gameTimeMulti = 0;

void initJeuMulti() {
	// clear window
	SDL_FillRect(win_surf, NULL, 0x000000);

	SDL_Rect tmpBtnBackMulti = { win_surf->w / 2 - BTN_W / 2, win_surf->h - 2 * BTN_H, BTN_H, BTN_W };
	btnBackMulti = tmpBtnBackMulti;
	SDL_Rect dstBtnBack = { btnBackMulti.x, btnBackMulti.y, btnBackMulti.h, btnBackMulti.w };
	SDL_FillRect(win_surf, &dstBtnBack, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("BACK", btnBackMulti.x + BTN_W / 4, btnBackMulti.y + BTN_H / 4);

	displayTerrainMulti();

	grilleJoueur = initGrille();
	grilleIa = initGrille();
}

void displayTerrainMulti() {
	int terrainW = GRILLE_COLUMN_NUMBER * TAILLE_BLOC;
	int terrainH = GRILLE_LINE_NUMBER * TAILLE_BLOC;
	int infoW = 0.66 * terrainW;

	// display player terrain
	SDL_Rect tmpTerrain = { win_surf->w / 2 - (2*terrainW + infoW) / 2, win_surf->w / 20, terrainW, terrainH + 2 * TAILLE_BLOC };
	terrainJoueur = tmpTerrain;

	SDL_Rect lineTerrainSup = { terrainJoueur.x - LINE_THICKNESS,  terrainJoueur.y - LINE_THICKNESS, terrainW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainSup, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainLeft = { lineTerrainSup.x,  lineTerrainSup.y, LINE_THICKNESS, terrainH + 2 * LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainLeft, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainInf = { lineTerrainSup.x + LINE_THICKNESS, lineTerrainSup.y + terrainH + LINE_THICKNESS, terrainW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainInf, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainRight = { lineTerrainSup.x + terrainW + LINE_THICKNESS, lineTerrainSup.y, LINE_THICKNESS, terrainH + LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainRight, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	// display info zone	
	SDL_Rect lineInfoSup = { lineTerrainRight.x + LINE_THICKNESS,  lineTerrainSup.y, infoW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineInfoSup, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineInfoInf = { lineTerrainRight.x + LINE_THICKNESS,  lineTerrainInf.y, infoW + 2 * LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineInfoInf, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineInfoRight = { lineInfoSup.x + infoW + LINE_THICKNESS,  lineTerrainRight.y, LINE_THICKNESS, terrainH + LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineInfoRight, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	// display next piece zone
	SDL_Rect nextPieceText = { lineInfoSup.x + infoW / 2 - 2 * CHAR_WIDTH, lineInfoSup.y + 2 * LINE_THICKNESS, 4 * CHAR_WIDTH, CHAR_WIDTH };
	displayText("NEXT", nextPieceText.x, nextPieceText.y);

	SDL_Rect tmpZoneNextPiece = { nextPieceText.x - CHAR_WIDTH / 2, nextPieceText.y + 2 * CHAR_WIDTH, 4 * TAILLE_BLOC, 2 * TAILLE_BLOC };
	zoneNextPiece = tmpZoneNextPiece;

	// display time
	SDL_Rect textTimeInfo = { nextPieceText.x, zoneNextPiece.y + 8 * CHAR_WIDTH, 4 * CHAR_WIDTH, CHAR_WIDTH };
	displayText("TIME", textTimeInfo.x, textTimeInfo.y);

	SDL_Rect tmpTextTime = { lineTerrainRight.x + LINE_THICKNESS * 2, textTimeInfo.y + 3 * CHAR_WIDTH, 10 * CHAR_WIDTH, CHAR_WIDTH };
	textTime = tmpTextTime;

	//display ai terrain
	SDL_Rect tmpTerrainAi = { lineInfoRight.x +LINE_THICKNESS, lineInfoRight.y + LINE_THICKNESS, terrainW, terrainH + 2 * TAILLE_BLOC };
	terrainIa = tmpTerrainAi;

	SDL_Rect lineTerrainAiSup = { terrainIa.x - LINE_THICKNESS,  terrainIa.y - LINE_THICKNESS, terrainW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainAiSup, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainAiInf = { lineTerrainAiSup.x + LINE_THICKNESS, lineTerrainAiSup.y + terrainH + LINE_THICKNESS, terrainW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainAiInf, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainAiRight = { lineTerrainAiSup.x + terrainW + LINE_THICKNESS, lineTerrainAiSup.y, LINE_THICKNESS, terrainH + LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainAiRight, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

}

void updateGameScreenMulti() {
	displayInt(gameTimeMulti, textTime.x, textTime.y);
	SDL_FillRect(win_surf, &zoneNextPiece, SDL_MapRGB(win_surf->format, 0, 0, 0));
	displayPiece(nextPieceJoueur.type, zoneNextPiece.x, zoneNextPiece.y);
	afficherGrille(grilleJoueur, terrainJoueur, win_surf);
	afficherGrille(grilleIa, terrainIa, win_surf);
	afficherPieceEnCours(pieceEnCoursJoueur, terrainJoueur.x, terrainJoueur.y, win_surf);
	afficherPieceEnCours(pieceEnCoursIa, terrainIa.x, terrainIa.y, win_surf);
	drawSeparationLines(terrainJoueur, win_surf);
	drawSeparationLines(terrainIa, win_surf);
	SDL_UpdateWindowSurface(pWindow);
}

void startJeuMulti(SDL_Surface* win_surf_base, SDL_Window* pWindow_base) {
	win_surf = win_surf_base;
	pWindow = pWindow_base;
	initJeuMulti();
	SDL_UpdateWindowSurface(pWindow);

	gameTimeMulti = 0;
	pieceEnCoursDePositionnementJoueur = 0;
	pieceEnCoursDePositionnementIa = 0;
	nextPieceJoueur = getRandomPiece();
	nextPieceIa = getRandomPiece();

	SDL_Delay(1000);

	struct Input_State input;
	ZERO_STRUCT(input);

	Piece pieceToMimic;
	int currentCompletedLinesJoueur = 0;
	int currentCompletedLinesIa = 0;
	// 1 if player wins, 2 if Ai wins
	int winner = 0;
	clock_t start = clock();
	bool back = false;
	bool skipEndDialog = false;
	while (!back)
	{
		//to change piece droping speed
		SDL_Delay(500);
		SDL_Event event;
		while (SDL_PollEvent(&event) != 0) {
			switch (event.type)
			{
			case SDL_QUIT:
				back = true;
				skipEndDialog = true;
				break;
			case SDL_MOUSEBUTTONUP:
				if (checkBackToMainMenu(event, btnBackMulti)) {
					back = true;
					skipEndDialog = true;
					SDL_FillRect(win_surf, NULL, 0x000000);
				}
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT:
					if (deplacerPiece(pieceEnCoursJoueur, pieceEnCoursJoueur.x - 1, pieceEnCoursJoueur.y, grilleJoueur) == 1) {
						pieceEnCoursJoueur.x = pieceEnCoursJoueur.x - 1;
					}
					break;
				case SDLK_RIGHT:
					if (deplacerPiece(pieceEnCoursJoueur, pieceEnCoursJoueur.x + 1, pieceEnCoursJoueur.y, grilleJoueur) == 1) {
						pieceEnCoursJoueur.x = pieceEnCoursJoueur.x + 1;
					}
					break;
				case SDLK_UP:
					printf("rotation");
					Piece pieceRotated = rotatePiece(pieceEnCoursJoueur);
					//printf("Piece rotated X: %i Y: %i \n", pieceRotated.x, pieceRotated.y);
					if (pieceEstPlacableGrille(pieceRotated, grilleJoueur) == 1) {
						pieceEnCoursJoueur = pieceRotated;
					}
					break;
				case SDLK_DOWN:
					if (deplacerPiece(pieceEnCoursJoueur, pieceEnCoursJoueur.x, pieceEnCoursJoueur.y + 1, grilleJoueur) == 1) {
						pieceEnCoursJoueur.y = pieceEnCoursJoueur.y + 1;
					}
					break;
				}
			default: break;
			}
		}

		
		

		if (pieceEnCoursDePositionnementJoueur == 0) {
			currentCompletedLinesJoueur = ligneComplete(&grilleJoueur);
			if (currentCompletedLinesJoueur > 0) {
				// give lines to Ia
			}
			pieceEnCoursDePositionnementJoueur = 1;
			pieceEnCoursJoueur = nextPieceJoueur;
			nextPieceJoueur = getRandomPiece();
			afficherPieceEnCours(pieceEnCoursJoueur, terrainJoueur.x, terrainJoueur.y, win_surf);
			// if new piece blocked => finish game
			if (deplacerPiece(pieceEnCoursJoueur, pieceEnCoursJoueur.x, pieceEnCoursJoueur.y + 1, grilleJoueur) == 0) {
				winner = 2;
				back = true;
			}
			//addPieceToGrille(pieceEnCours);
		}
		if (pieceEnCoursDePositionnementIa == 0) {
			currentCompletedLinesIa = ligneComplete(&grilleIa);
			if (currentCompletedLinesIa > 0) {
				// give lines to Player
			}
			pieceEnCoursDePositionnementIa = 1;
			pieceEnCoursIa = nextPieceIa;
			nextPieceIa = getRandomPiece();
			pieceToMimic = getBestPieceByScore(&pieceEnCoursIa, grilleIa);
			afficherPieceEnCours(pieceEnCoursIa, terrainIa.x, terrainIa.y, win_surf);
			// if new piece blocked => finish game
			if (deplacerPiece(pieceEnCoursIa, pieceEnCoursIa.x, pieceEnCoursIa.y + 1, grilleIa) == 0) {
				winner = 1;
				back = true;
			}
			//addPieceToGrille(pieceEnCours);
		}
		else {
			pieceEnCoursJoueur = deplacementAutoPiece(pieceEnCoursJoueur, &grilleJoueur, &pieceEnCoursDePositionnementJoueur);
			pieceEnCoursIa = deplacementAutoPiece(pieceEnCoursIa, &grilleIa, &pieceEnCoursDePositionnementIa);
		}

		aiMovesPiece(&pieceEnCoursIa, grilleIa, pieceToMimic);

		clock_t end = clock();
		gameTimeMulti = (end - start) / CLOCKS_PER_SEC;

		//showGrille(grille);

		// refresh display
		updateGameScreenMulti();
	}

	if (!skipEndDialog) {
		// display end game dialog
		//ask to restart or quit game
		SDL_Rect dialogBox = { win_surf->w / 3, win_surf->h / 3, win_surf->w / 3,  win_surf->h / 3 };
		SDL_FillRect(win_surf, &dialogBox, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
		if (winner == 1) {
			displayText("PLAYER WINS", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 2 * CHAR_WIDTH);
		}
		else if (winner == 2) {
			displayText("AI WINS", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 2 * CHAR_WIDTH);
		}
		else {
			displayText("NO WINNER", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 2 * CHAR_WIDTH);
		}
		displayText("SPACE TO RESTART", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 4 * CHAR_WIDTH);
		displayText("BACKSPACE TO QUIT", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 6 * CHAR_WIDTH);
		SDL_UpdateWindowSurface(pWindow);

		int choice = -1;
		while (choice == -1) {
			SDL_Event event;
			while (SDL_PollEvent(&event) != 0) {
				switch (event.type)
				{
				case SDL_QUIT:
					choice = 0;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_BACKSPACE:
						choice = 0;
						break;
					case SDLK_SPACE:
						choice = 1;
					default:
						break;
					}
				default:
					break;
				}
				printf("choice %d", choice);
			}
		}
		if (choice == 1) {
			startJeuMulti(win_surf, pWindow);
		}
	}
	

	// clear window
	SDL_FillRect(win_surf, NULL, 0x000000);

	
}