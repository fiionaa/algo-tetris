#include "Scores.h"

#define NUM_SCORES 5
#define SCORE_CHARS_LIMIT 30
int scores[NUM_SCORES];
int used_scores = 5;

SDL_Rect btnBackScores;
SDL_Surface* win_surf;
SDL_Window* pWindow;

void initScores() {
	// clear window
	SDL_FillRect(win_surf, NULL, 0x000000);
	SDL_Rect tmpBtnBackScores = { win_surf->w / 2 - BTN_W / 2, win_surf->h - 2 * BTN_H, BTN_H, BTN_W };
	btnBackScores = tmpBtnBackScores;
	SDL_Rect dstBtnBack = { btnBackScores.x, btnBackScores.y, btnBackScores.h, btnBackScores.w };
	SDL_FillRect(win_surf, &dstBtnBack, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("BACK", btnBackScores.x + BTN_W / 4, btnBackScores.y + BTN_H / 4);

	char score_chars[SCORE_CHARS_LIMIT];

	for (int i = 0; i < used_scores; i++) {
		snprintf(score_chars, SCORE_CHARS_LIMIT, "%d", scores[i]);
		SDL_Rect rectScore = { btnBackScores.x, (i+1)* 2*btnBackScores.w,  btnBackScores.h,  btnBackScores.w };
		SDL_FillRect(win_surf, &rectScore, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
		displayInt(i+1, rectScore.x - BTN_W/2, rectScore.y + BTN_H / 4);
		displayInt(scores[i], rectScore.x + BTN_W / 4, rectScore.y + BTN_H / 4);
	}
}

void showScores(SDL_Surface* win_surf_base, SDL_Window* pWindow_base) {
	win_surf = win_surf_base;
	pWindow = pWindow_base;
	initScores();
	SDL_UpdateWindowSurface(pWindow);

	bool back = false;
	while (!back)
	{
		SDL_Event event;
		while (!back && SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				back = true;
				break;
			case SDL_MOUSEBUTTONUP:
				if (checkBackToMainMenu(event, btnBackScores)) {
					back = true;
					SDL_FillRect(win_surf, NULL, 0x000000);
				}
				break;
			default: break;
			}
		}
		SDL_PumpEvents();
	}
}

void addScore(int score) {
	for (int i = 0; i < used_scores; i++) {
		if (scores[i] < score) {
			int aux = scores[i];
			scores[i] = score;
			score = aux;
		}
	}
	if (used_scores < NUM_SCORES) {
		scores[used_scores] = score;
		used_scores++;
	}
}