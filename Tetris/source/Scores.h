#ifndef SCORES_H
#define SCORES_H

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "utilsBoutons.h"
#include "utilsAffichage.h"

void initScores();
void showScores(SDL_Surface* win_surf_base, SDL_Window* pWindow_base);
void addScore(int score);

#endif