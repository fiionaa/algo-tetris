﻿#ifndef TETRIS_H
#define TETRIS_H

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <windows.h>
//#include <SDL_mixer.h>
//#include <SDL_ttf.h>
#include "Point.h"
#include "Piece.h"
#include "JeuSolo.h"
#include "JeuMulti.h"
#include "Scores.h"
#include "utilsBoutons.h"
#include "utilsAffichage.h"
#include "Grille.h"
//#include "music.h"


void initWindow();
void initMainMenu();
void checkMainMenuEvent(SDL_Event event);

#endif