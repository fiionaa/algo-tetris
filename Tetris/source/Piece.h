#ifndef PIECE_H
#define PIECE_H
#include "Grille.h"
#include <stdio.h>


typedef struct Piece Piece;
struct Piece {
	int x;
	int y;
	int rotation;
	int forme[4][2][2];
	char* nom;
	int red;
	int green;
	int blue;
	int type;
};

typedef enum
{
	BARRE,
	BLOC,
	TE,
	LAMBDA,
	GAMMA,
	BIAIS,
	BIAIS_INVERSE
} TypePiece;

typedef enum {
	VIDE = -99999,
	VIDEROTATED = 99999
} TypeCase;
Piece new_piece(int x, int y, TypePiece type);
Piece rotatePiece(Piece piece);
Piece getRandomPiece();
#endif