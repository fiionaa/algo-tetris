#ifndef JEU_H
#define JEU_H

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <math.h>
#include "utilsBoutons.h"
#include "utilsAffichage.h"
#include "Grille.h"
#include "Piece.h"
#include "Scores.h"

#define ZERO_STRUCT(obj) memset(&(obj), 0, sizeof(obj))
#define LINE_THICKNESS 10
#define TAILLE_BLOC 25
static Piece pieces[25];

struct Input_State
{
	uint8_t left;
	uint8_t right;
	uint8_t up;
	uint8_t down;

	uint8_t a;

	int8_t dleft;
	int8_t dright;
	int8_t dup;
	int8_t ddown;
	int8_t da;
};

Grille initGrille();
void afficherGrille(Grille grille, SDL_Rect terrain, SDL_Surface* win_surf);
void drawSeparationLines(terrain, win_surf);
int deplacerPiece(Piece piece, int newX, int newY, Grille grille);
Piece deplacementAutoPiece(Piece piece, Grille* grille, int* pieceEnCoursDePositionnement);
int pieceEstPlacableGrille(Piece piece, Grille grille);
Grille addPieceToGrille(Piece piece, Grille grille);
void afficherPieceEnCours(Piece piece, int terrainX, int terrainY, SDL_Surface* win_surf);
int ligneComplete(Grille* grille);
int scoreByLines(int linesMultiplier);
void updateGameLevel(int* gameLevel, int gameLinesScored);
void updateScore(int linesMultiplier, int gameLevel, int* gameLinesScored, int* gameScore);
void putBlockingLinesInGrille(int blockingLines, Grille* grille);

#endif