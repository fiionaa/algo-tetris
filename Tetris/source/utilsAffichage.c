#include "utilsAffichage.h"

const int BG_HEIGHT = 320;
const int BG_WIDTH = 232;

SDL_Surface* plancheSpritesBackground = NULL;
SDL_Surface* plancheSpritesForme = NULL;
SDL_Surface* plancheSpritesTexte = NULL;
SDL_Surface* win_surf;

void initSpritesSurfaces(char* backgroundsPath, char* spritesPath, char* textPath, SDL_Surface* win_surf_base) {
	win_surf = win_surf_base;

	plancheSpritesBackground = SDL_LoadBMP("./tetris_backgrounds.bmp");
	plancheSpritesForme = SDL_LoadBMP("./tetrinos.bmp");
	plancheSpritesTexte = SDL_LoadBMP("./tetris_text.bmp");

	SDL_SetColorKey(plancheSpritesBackground, false, 0);  // 0: 00/00/00 noir -> transparent
	SDL_SetColorKey(plancheSpritesForme, false, 0);
	SDL_SetColorKey(plancheSpritesTexte, false, 0);
}

void displayRandomBackground() {
	SDL_Rect srcBg = { 1280,232, BG_HEIGHT, BG_WIDTH };
	SDL_Rect dest = { 0,0,0,0 };
	SDL_BlitSurface(plancheSpritesBackground, &srcBg, win_surf, &dest);
}

void displayPiece(TypePiece type, int xdst, int ydst) {
	SDL_Rect srcPiece = { 0 };
	switch (type)
	{
		case BARRE:
			srcPiece.x = 0;
			srcPiece.y = 0;
			srcPiece.h = 25;
			srcPiece.w = 100;
			break;
		case BLOC:
			srcPiece.x = 175;
			srcPiece.y = 0;
			srcPiece.h = 50;
			srcPiece.w = 50;
			break;
		case TE:
			srcPiece.x = 0;
			srcPiece.y = 225;
			srcPiece.h = 50;
			srcPiece.w = 75;
			break;
		case LAMBDA:
			srcPiece.x = 350;
			srcPiece.y = 125;
			srcPiece.h = 50;
			srcPiece.w = 75;
			break;
		case GAMMA:
			srcPiece.x = 0;
			srcPiece.y = 125;
			srcPiece.h = 50;
			srcPiece.w = 75;
			break;
		case BIAIS:
			srcPiece.x = 425;
			srcPiece.y = 0;
			srcPiece.h = 50;
			srcPiece.w = 75;
			break;
		case BIAIS_INVERSE:
			srcPiece.x = 250;
			srcPiece.y = 0;
			srcPiece.h = 50;
			srcPiece.w = 75;
			break;
		default:
			break;
	}
	SDL_Rect dest = { xdst,ydst,0,0 };
	SDL_BlitSurface(plancheSpritesForme, &srcPiece, win_surf, &dest);
}

void displayText(char* text, const int xOffset, const int yOffset) {

	for (int i = 0; i < (strlen(text)); i++)
	{
		SDL_Rect dest = { 0,0,0,0 };

		dest.x = (text[i] - ' ') % 16;
		dest.y = (text[i] - ' ') / 16;

		SDL_Rect srcLetter = { dest.x * DEFAULT_SIZE, dest.y * DEFAULT_SIZE, DEFAULT_SIZE, DEFAULT_SIZE };

		dest.x = xOffset + i * CHAR_WIDTH;
		dest.y = yOffset;

		SDL_BlitSurface(plancheSpritesTexte, &srcLetter, win_surf, &dest);
	}

}

void displayInt(int value, const int xOffset, const int yOffset) {
	if (value > 0)
	{
		char valueToDisplay = (value % 10) + '0';

		displayInt(value / 10, xOffset, yOffset);
		displayText(&valueToDisplay, xOffset + (floor(log10(abs(value)))) * CHAR_WIDTH, yOffset);
	}
	else if (value == 0) {
		displayText("0", xOffset, yOffset);
	}
}