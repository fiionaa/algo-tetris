#ifndef IA_H
#define IA_H
#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "utilsBoutons.h"
#include "utilsAffichage.h"
#include "Grille.h"
#include "Piece.h"
#include "Jeu.h"

#define HEIGHT_WEIGHT 0.510066
#define LINES_WEIGHT 0.760666
#define HOLES_WEIGHT 0.35663
#define BUMPINESS_WEIGHT 0.184483

void aiMovesPiece(Piece* pieceToMove, Grille grille, Piece pieceToMimic);
Piece getBestPieceByScore(Piece* pieceToMove, Grille grille);
bool aiMovesPieceLeft(Piece* pieceToMove, Grille grille);
bool aiMovesPieceRight(Piece* pieceToMove, Grille grille);
bool aiRotatesPiece(Piece* pieceToMove, Grille grille);
bool aiMovesPieceDown(Piece* pieceToMove, Grille grille);

#endif