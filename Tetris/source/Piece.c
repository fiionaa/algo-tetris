#include "Piece.h"

Piece new_piece(int x, int y, TypePiece type) {

	Piece piece;
	piece.x = x;
	piece.y = y;
	piece.rotation = 0;
	piece.type = type;
	switch (type) {
		case BARRE:
			piece.forme[0][0][0] = 0;
			piece.forme[0][0][1] = -2;
			piece.forme[0][1][0] = VIDE;
			piece.forme[0][1][1] = VIDE;

			piece.forme[1][0][0] = 0;
			piece.forme[1][0][1] = -1;
			piece.forme[1][1][0] = VIDE;
			piece.forme[1][1][1] = VIDE;

			piece.forme[2][0][0] = 0;
			piece.forme[2][0][1] = 0;
			piece.forme[2][1][0] = VIDE;
			piece.forme[2][1][1] = VIDE;

			piece.forme[3][0][0] = 0;
			piece.forme[3][0][1] = 1;
			piece.forme[3][1][0] = VIDE;
			piece.forme[3][1][1] = VIDE;
			piece.red = 0;
			piece.green = 255;
			piece.blue = 255;
			break;
		case BLOC: 
			piece.forme[0][0][0] = VIDE;
			piece.forme[0][0][1] = VIDE;
			piece.forme[0][1][0] = VIDE;
			piece.forme[0][1][1] = VIDE;

			piece.forme[1][0][0] = VIDE;
			piece.forme[1][0][1] = VIDE;
			piece.forme[1][1][0] = VIDE;
			piece.forme[1][1][1] = VIDE;

			piece.forme[2][0][0] = 0;
			piece.forme[2][0][1] = 1;
			piece.forme[2][1][0] = 1;
			piece.forme[2][1][1] = 1;

			piece.forme[3][0][0] = 0;
			piece.forme[3][0][1] = 0;
			piece.forme[3][1][0] = 1;
			piece.forme[3][1][1] = 0;
			piece.red = 255;
			piece.green = 255;
			piece.blue = 0;
			break;
		case TE:
			piece.forme[0][0][0] = 0;
			piece.forme[0][0][1] = 0;
			piece.forme[0][1][0] = VIDE;
			piece.forme[0][1][1] = VIDE;

			piece.forme[1][0][0] = 1;
			piece.forme[1][0][1] = 0;
			piece.forme[1][1][0] = 1;
			piece.forme[1][1][1] = 1;

			piece.forme[2][0][0] = 2;
			piece.forme[2][0][1] = 0;
			piece.forme[2][1][0] = VIDE;
			piece.forme[2][1][1] = VIDE;

			piece.forme[3][0][0] = VIDE;
			piece.forme[3][0][1] = VIDE;
			piece.forme[3][1][0] = VIDE;
			piece.forme[3][1][1] = VIDE;
			piece.red = 128;
			piece.green = 0;
			piece.blue = 255;
			break;
		case LAMBDA:
			piece.forme[0][0][0] = -1;
			piece.forme[0][0][1] = 0;
			piece.forme[0][1][0] = 0;
			piece.forme[0][1][1] = 0;

			piece.forme[1][0][0] = -1;
			piece.forme[1][0][1] = -1;
			piece.forme[1][1][0] = VIDE;
			piece.forme[1][1][1] = VIDE;

			piece.forme[2][0][0] = -1;
			piece.forme[2][0][1] = -2;
			piece.forme[2][1][0] = VIDE;
			piece.forme[2][1][1] = VIDE;

			piece.forme[3][0][0] = -1;
			piece.forme[3][0][1] = -3;
			piece.forme[3][1][0] = VIDE;
			piece.forme[3][0][0] = VIDE;

			piece.red = 255;
			piece.green = 128;
			piece.blue = 0;
			break;
		case GAMMA:
			piece.forme[0][0][0] = -1;
			piece.forme[0][0][1] = 3;
			piece.forme[0][1][0] = VIDE;
			piece.forme[0][1][1] = VIDE;

			piece.forme[1][0][0] = -1;
			piece.forme[1][0][1] = 2;
			piece.forme[1][1][0] = VIDE;
			piece.forme[1][1][1] = VIDE;

			piece.forme[2][0][0] = -1;
			piece.forme[2][0][1] = 1;
			piece.forme[2][1][0] = VIDE;
			piece.forme[2][1][1] = VIDE;

			piece.forme[3][0][0] = -1;
			piece.forme[3][0][1] = 0;
			piece.forme[3][1][0] = 0;
			piece.forme[3][1][1] = 0;
			piece.red = 0;
			piece.green = 0;
			piece.blue = 255;
			break;
		case BIAIS:
			piece.forme[0][0][0] = VIDE;
			piece.forme[0][0][1] = VIDE;
			piece.forme[0][1][0] = 0;
			piece.forme[0][1][1] = 0;

			piece.forme[1][0][0] = -1;
			piece.forme[1][0][1] = -1;
			piece.forme[1][1][0] = 0;
			piece.forme[1][1][1] = -1;

			piece.forme[2][0][0] = -1;
			piece.forme[2][0][1] = -2;
			piece.forme[2][1][0] = VIDE;
			piece.forme[2][1][1] = VIDE;

			piece.forme[3][0][0] = VIDE;
			piece.forme[3][0][1] = VIDE;
			piece.forme[3][1][0] = VIDE;
			piece.forme[3][1][1] = VIDE;

			piece.red = 255;
			piece.green = 0;
			piece.blue = 0;
			break;
		case BIAIS_INVERSE:
			piece.forme[0][0][0] = 0;
			piece.forme[0][0][1] = 0;
			piece.forme[0][1][0] = VIDE;
			piece.forme[0][1][1] = VIDE;

			piece.forme[1][0][0] = 0;
			piece.forme[1][0][1] = -1;
			piece.forme[1][1][0] = 1;
			piece.forme[1][1][1] = -1;

			piece.forme[2][0][0] = VIDE;
			piece.forme[2][0][1] = VIDE;
			piece.forme[2][1][0] = 1;
			piece.forme[2][1][1] = -2;

			piece.forme[3][0][0] = VIDE;
			piece.forme[3][0][1] = VIDE;
			piece.forme[3][1][0] = VIDE;
			piece.forme[3][1][1] = VIDE;

			piece.red = 0;
			piece.green = 255;
			piece.blue = 0;
			break;
	}
	return piece;
}
Piece rotatePiece(Piece piece) {
	if (piece.type == BARRE) {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				if (piece.forme[i][j][0] != VIDE && piece.forme[i][j][1] != VIDE) {
					int x = piece.forme[i][j][0];
					int y = piece.forme[i][j][1];
					piece.forme[i][j][0] = y;
					piece.forme[i][j][1] = x;
				}
			}
		}
	}
	else {
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				int x = piece.forme[i][j][0];
				int y = piece.forme[i][j][1];
				piece.forme[i][j][0] = y;
				piece.forme[i][j][1] = x * -1;
			}
		}
	}
	if (piece.rotation == 3) {
		piece.rotation = 0;
	}
	else {
		piece.rotation++;
	}
	return piece;
}
Piece getRandomPiece() {
	return new_piece((GRILLE_COLUMN_NUMBER-1)/2, 2, rand() % 7);
}