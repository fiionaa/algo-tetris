#ifndef UTILSBOUTONS_H
#define UTILSBOUTONS_H

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define BTN_W 200
#define BTN_H 50

bool checkBackToMainMenu(SDL_Event event, SDL_Rect btnBack);
bool checkQuitGame(SDL_Event event, SDL_Rect btnQuit);

#endif