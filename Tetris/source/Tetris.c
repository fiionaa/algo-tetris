﻿#include "Tetris.h"

SDL_Rect btnStartMarathon;
SDL_Rect btnStartVersus;
SDL_Rect btnScore;
SDL_Rect btnQuit;

SDL_Window* pWindow = NULL;
SDL_Surface* win_surf = NULL;

void initWindow()
{
	// création de la fenêtre
	pWindow = SDL_CreateWindow("Tetris", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 1200, 800, SDL_WINDOW_SHOWN);
	win_surf = SDL_GetWindowSurface(pWindow);
	// définition des planches de sprites
	initSpritesSurfaces("./tetris_backgrounds.bmp", "./tetris_sprites.bmp", "./tetris_text.bmp", win_surf);
}

void initMainMenu()
{	
	// crée les boutons
	SDL_Rect tmpBtnStartMarathon = { win_surf->w / 2 - BTN_W/2, win_surf->h / 2 - 4*BTN_H, BTN_W, BTN_H };
	SDL_Rect tmpBtnStartVersus = { win_surf->w / 2 - BTN_W / 2, win_surf->h / 2 - 2*BTN_H , BTN_H, BTN_W };
	SDL_Rect tmpBtnScore = { win_surf->w / 2 - BTN_W / 2, win_surf->h / 2, BTN_H, BTN_W };
	SDL_Rect tmpBtnQuit = { win_surf->w / 2 - BTN_W / 2, win_surf->h / 2 + 2*BTN_H, BTN_H, BTN_W };

	btnStartMarathon = tmpBtnStartMarathon;
	btnStartVersus = tmpBtnStartVersus;
	btnScore = tmpBtnScore;
	btnQuit = tmpBtnQuit;

	//affiche les boutons
	SDL_Rect dstBtnStartMarathon = { btnStartMarathon.x, btnStartMarathon.y, btnStartMarathon.h, btnStartMarathon.w };
	SDL_FillRect(win_surf, &btnStartMarathon, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("SOLO", btnStartMarathon.x + BTN_W / 4, btnStartMarathon.y + BTN_H / 4);

	SDL_Rect dstBtnStartVersus = { btnStartVersus.x, btnStartVersus.y, btnStartVersus.h, btnStartVersus.w };
	SDL_FillRect(win_surf, &dstBtnStartVersus, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("VERSUS", btnStartVersus.x + BTN_W / 4, btnStartVersus.y + BTN_H / 4);

	SDL_Rect dstBtnScore = { btnScore.x, btnScore.y, btnScore.h, btnScore.w };
	SDL_FillRect(win_surf, &dstBtnScore, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("SCORE", btnScore.x + BTN_W / 4, btnScore.y + BTN_H / 4);

	SDL_Rect dstBtnQuit = { btnQuit.x, btnQuit.y, btnQuit.h, btnQuit.w };
	SDL_FillRect(win_surf, &dstBtnQuit, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("QUIT", btnQuit.x + BTN_W / 4, btnQuit.y + BTN_H / 4);
}

void checkMainMenuEvent(SDL_Event event) {
	if (event.button.button == SDL_BUTTON_LEFT) {
		if (event.button.x > btnStartMarathon.x &&
			event.button.x < btnStartMarathon.x + btnQuit.h &&
			event.button.y > btnStartMarathon.y &&
			event.button.y < btnStartMarathon.y + btnQuit.w)
		{
			startJeuSolo(win_surf, pWindow);
			// re init main menu when exit
			initMainMenu();
		}
		else if (event.button.x > btnStartVersus.x &&
			event.button.x < btnStartVersus.x + btnQuit.h &&
			event.button.y > btnStartVersus.y &&
			event.button.y < btnStartVersus.y + btnQuit.w)
		{
			startJeuMulti(win_surf, pWindow);
			// re init main menu when exit
			initMainMenu();
		}
		else if (event.button.x > btnScore.x &&
			event.button.x < btnScore.x + btnQuit.h &&
			event.button.y > btnScore.y &&
			event.button.y < btnScore.y + btnQuit.w)
		{
			showScores(win_surf, pWindow);
			// re init main menu when exit
			initMainMenu();
		}
	}
}

int main(int argc, char** argv)
{
	if (SDL_Init(SDL_INIT_VIDEO) != 0)
	{
		return 1;
	}
	initWindow();
	initMainMenu();

	bool quit = false;

	while (!quit)
	{
		SDL_Event event;
		while (!quit && SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				quit = true;
				break;
			case SDL_MOUSEBUTTONUP:
				if (checkQuitGame(event, btnQuit)) {
					quit = true;
				}
				else {
					checkMainMenuEvent(event);
				}
				break;
			default: break;
			}
		}
		SDL_PumpEvents();

		const Uint8* state = SDL_GetKeyboardState(NULL);
		quit |= state[SDL_SCANCODE_ESCAPE];
		
		SDL_UpdateWindowSurface(pWindow);
	}
	SDL_Quit();
	return 0;
}
