 #include "JeuSolo.h"

SDL_Rect btnBackSolo;
SDL_Surface* win_surf;
SDL_Window* pWindow;

SDL_Rect terrain;
SDL_Rect textScore;
SDL_Rect textTime;
SDL_Rect zoneNextPiece;
SDL_Rect textLevel;

Grille grille;
Piece pieceEnCours;
Piece nextPiece;
int pieceEnCoursDePositionnement = 0;

int gameScore = 0;
int gameLevel = 0;
int gameTime = 0;
int gameLinesScored = 0;

void initJeuSolo() {
	// clear window
	SDL_FillRect(win_surf, NULL, 0x000000);

	SDL_Rect tmpBtnBackSolo = { win_surf->w / 2 - BTN_W / 2, win_surf->h - 2 * BTN_H, BTN_H, BTN_W };
	btnBackSolo = tmpBtnBackSolo;
	SDL_Rect dstBtnBack = { btnBackSolo.x, btnBackSolo.y, btnBackSolo.h, btnBackSolo.w };
	SDL_FillRect(win_surf, &dstBtnBack, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	displayText("BACK", btnBackSolo.x + BTN_W / 4, btnBackSolo.y + BTN_H / 4);

	displayTerrainSolo();

	grille = initGrille();
}

void displayTerrainSolo() {
	int terrainW = GRILLE_COLUMN_NUMBER * TAILLE_BLOC;
	int terrainH = GRILLE_LINE_NUMBER * TAILLE_BLOC;
	int infoW = 0.66 * terrainW;

	// display terrain
	SDL_Rect tmpTerrain = { win_surf->w / 2 - (terrainW+infoW) / 2, win_surf->w / 20, terrainW, terrainH+2*TAILLE_BLOC };
	terrain = tmpTerrain;

	SDL_Rect lineTerrainSup = { terrain.x - LINE_THICKNESS,  terrain.y- LINE_THICKNESS, terrainW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainSup, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainLeft = { lineTerrainSup.x,  lineTerrainSup.y, LINE_THICKNESS, terrainH + 2*LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainLeft, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainInf = { lineTerrainSup.x + LINE_THICKNESS, lineTerrainSup.y + terrainH + LINE_THICKNESS, terrainW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainInf, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineTerrainRight = { lineTerrainSup.x + terrainW + LINE_THICKNESS, lineTerrainSup.y, LINE_THICKNESS, terrainH + LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineTerrainRight, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	// display info zone	
	SDL_Rect lineInfoSup = { lineTerrainRight.x + LINE_THICKNESS,  lineTerrainSup.y, infoW + LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineInfoSup, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
	
	SDL_Rect lineInfoInf = { lineTerrainRight.x + LINE_THICKNESS,  lineTerrainInf.y, infoW + 2*LINE_THICKNESS, LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineInfoInf, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	SDL_Rect lineInfoRight = { lineInfoSup.x + infoW + LINE_THICKNESS,  lineTerrainRight.y, LINE_THICKNESS, terrainH + LINE_THICKNESS };
	SDL_FillRect(win_surf, &lineInfoRight, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));

	// display next piece zone
	SDL_Rect nextPieceText = { lineInfoSup.x + infoW/2 - 2*CHAR_WIDTH, lineInfoSup.y + 2 * LINE_THICKNESS, 4*CHAR_WIDTH, CHAR_WIDTH };
	displayText("NEXT", nextPieceText.x, nextPieceText.y);

	SDL_Rect tmpZoneNextPiece = { nextPieceText.x - CHAR_WIDTH/2, nextPieceText.y + 2*CHAR_WIDTH, 4 * TAILLE_BLOC, 2*TAILLE_BLOC };
	zoneNextPiece = tmpZoneNextPiece;

	// display text score	
	SDL_Rect textScoreInfo = { nextPieceText.x, zoneNextPiece.y + 3 * TAILLE_BLOC, 5 * CHAR_WIDTH, CHAR_WIDTH };
	displayText("SCORE", textScoreInfo.x, textScoreInfo.y);

	SDL_Rect tmpTextScore = { lineTerrainRight.x + LINE_THICKNESS*2, textScoreInfo.y + 3 * CHAR_WIDTH, 10 * CHAR_WIDTH, CHAR_WIDTH };
	textScore = tmpTextScore;

	// display time
	SDL_Rect textTimeInfo = { textScoreInfo.x, textScore.y + 4 * CHAR_WIDTH, 4 * CHAR_WIDTH, CHAR_WIDTH };
	displayText("TIME", textTimeInfo.x, textTimeInfo.y);

	SDL_Rect tmpTextTime = { lineTerrainRight.x + LINE_THICKNESS*2, textTimeInfo.y + 3 * CHAR_WIDTH, 10 * CHAR_WIDTH, CHAR_WIDTH };
	textTime = tmpTextTime;

	// display level
	SDL_Rect textLevelInfo = { textScoreInfo.x, textTime.y + 4 * CHAR_WIDTH, 4 * CHAR_WIDTH, CHAR_WIDTH };
	displayText("LEVEL", textLevelInfo.x, textLevelInfo.y);

	SDL_Rect tmpTextLevel = { lineTerrainRight.x + LINE_THICKNESS*2, textLevelInfo.y + 3 * CHAR_WIDTH, 10 * CHAR_WIDTH, CHAR_WIDTH };
	textLevel = tmpTextLevel;
}

void updateGameScreenSolo() {
	displayInt(gameScore, textScore.x, textScore.y);
	displayInt(gameTime, textTime.x, textTime.y);
	displayInt(gameLevel, textLevel.x, textLevel.y);
	SDL_FillRect(win_surf, &zoneNextPiece, SDL_MapRGB(win_surf->format, 0, 0, 0));
	displayPiece(nextPiece.type, zoneNextPiece.x, zoneNextPiece.y);
	afficherGrille(grille, terrain, win_surf);
	afficherPieceEnCours(pieceEnCours, terrain.x, terrain.y, win_surf);
	drawSeparationLines(terrain, win_surf);
	SDL_UpdateWindowSurface(pWindow);
}

void startJeuSolo(SDL_Surface* win_surf_base, SDL_Window* pWindow_base) {
	win_surf = win_surf_base;
	pWindow = pWindow_base;
	initJeuSolo();
	SDL_UpdateWindowSurface(pWindow);

	gameScore = 0;
	gameTime = 0;
	gameLevel = 0;
	gameLinesScored = 0;
	pieceEnCoursDePositionnement = 0;
	nextPiece = getRandomPiece();
	SDL_Delay(1000);

	struct Input_State input;
	ZERO_STRUCT(input);
	
	int currentCompletedLines = 0;
	clock_t start = clock();
	bool back = false;
	bool skipEndDialog = false;
	while (!back)
	{
		//to change piece droping speed
		SDL_Delay(500/(gameLevel+1));
		SDL_Event event;
		while (SDL_PollEvent(&event) != 0) {
			switch (event.type)
			{
			case SDL_QUIT:
				back = true;
				skipEndDialog = true;
				break;
			case SDL_MOUSEBUTTONUP:
				if (checkBackToMainMenu(event, btnBackSolo)) {
					back = true;
					skipEndDialog = true;
					SDL_FillRect(win_surf, NULL, 0x000000);
				}
				break;
			case SDL_KEYDOWN:
				switch (event.key.keysym.sym)
				{
				case SDLK_LEFT: 
					if (deplacerPiece(pieceEnCours, pieceEnCours.x - 1, pieceEnCours.y, grille) == 1) {
						pieceEnCours.x = pieceEnCours.x - 1;
					}
					break;
				case SDLK_RIGHT:
					if (deplacerPiece(pieceEnCours, pieceEnCours.x + 1, pieceEnCours.y, grille) == 1) {
						pieceEnCours.x = pieceEnCours.x + 1;
					}
					break;
				case SDLK_UP:    
					printf("rotate");
					Piece pieceRotated = rotatePiece(pieceEnCours);
					//printf("Piece rotated X: %i Y: %i \n", pieceRotated.x, pieceRotated.y);
					if (pieceEstPlacableGrille(pieceRotated, grille) == 1) {
						pieceEnCours = pieceRotated;
					}
					break;
				case SDLK_DOWN:
					if (deplacerPiece(pieceEnCours, pieceEnCours.x, pieceEnCours.y + 1, grille) == 1) {
						pieceEnCours.y = pieceEnCours.y + 1;
					}
					break;
				}
			default: break;
			}
		}
		
		if (pieceEnCoursDePositionnement == 0) {
			currentCompletedLines = ligneComplete(&grille);
			if (currentCompletedLines > 0) {				
				updateScore(currentCompletedLines, gameLevel, &gameLinesScored, &gameScore);
				currentCompletedLines = 0;
			}
			pieceEnCoursDePositionnement = 1;
			pieceEnCours = nextPiece;
			nextPiece = getRandomPiece();			
			afficherPieceEnCours(pieceEnCours, terrain.x, terrain.y, win_surf);
			// if new piece blocked => finish game
			if (deplacerPiece(pieceEnCours, pieceEnCours.x, pieceEnCours.y + 1, grille) == 0) {
				back = true;
			}
			//addPieceToGrille(pieceEnCours);
		}
		else {
			pieceEnCours = deplacementAutoPiece(pieceEnCours, &grille, &pieceEnCoursDePositionnement);
		}
		clock_t end = clock();
		gameTime = (end - start) / CLOCKS_PER_SEC;
		showGrille(grille);
		
		//change game level if needed
		updateGameLevel(&gameLevel, gameLinesScored);
		// refresh display
		updateGameScreenSolo();
	}

	//add score
	addScore(gameScore);
	if (!skipEndDialog) {
		// display end game dialog
		//ask to restart or quit game
		SDL_Rect dialogBox = { win_surf->w / 3, win_surf->h / 3, win_surf->w / 3,  win_surf->h / 3 };
		SDL_FillRect(win_surf, &dialogBox, SDL_MapRGB(win_surf->format, 0x80, 0x80, 0x80));
		displayText("SCORE", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 2 * CHAR_WIDTH);
		displayInt(gameScore, dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 4 * CHAR_WIDTH);
		displayText("SPACE TO RESTART", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 6 * CHAR_WIDTH);
		displayText("BACKSPACE TO QUIT", dialogBox.x + 2 * CHAR_WIDTH, dialogBox.y + 8 * CHAR_WIDTH);
		SDL_UpdateWindowSurface(pWindow);

		int choice = -1;
		while (choice == -1) {
			SDL_Event event;
			while (SDL_PollEvent(&event) != 0) {
				switch (event.type)
				{
				case SDL_QUIT:
					choice = 0;
					break;
				case SDL_KEYDOWN:
					switch (event.key.keysym.sym)
					{
					case SDLK_BACKSPACE:
						choice = 0;
						break;
					case SDLK_SPACE:
						choice = 1;
					default:
						break;
					}
				default:
					break;
				}
				printf("choice %d", choice);
			}
		}
		if (choice == 1) {
			startJeuSolo(win_surf, pWindow);
		}
	}

	// clear window
	SDL_FillRect(win_surf, NULL, 0x000000);	
}