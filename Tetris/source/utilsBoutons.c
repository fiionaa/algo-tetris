#include "utilsBoutons.h"

bool checkBackToMainMenu(SDL_Event event, SDL_Rect btnBack) {
	if (event.button.button == SDL_BUTTON_LEFT) {
		if (event.button.x > btnBack.x &&
			event.button.x < btnBack.x + btnBack.h &&
			event.button.y > btnBack.y &&
			event.button.y < btnBack.y + btnBack.w)
		{			
			return true;
		}
	}
	return false;
}

bool checkQuitGame(SDL_Event event, SDL_Rect btnQuit) {
	if (event.button.button == SDL_BUTTON_LEFT) {
		if (event.button.x > btnQuit.x &&
			event.button.x < btnQuit.x + btnQuit.h &&
			event.button.y > btnQuit.y &&
			event.button.y < btnQuit.y + btnQuit.w)
		{
			return true;
		}
	}
	return false;
}