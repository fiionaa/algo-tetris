#ifndef GRILLE_H
#define GRILLE_H
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
//type de cellules de la grille

#define GRILLE_LINE_NUMBER 20
#define GRILLE_COLUMN_NUMBER 10

typedef enum
{
	CARRE_BLEU,
	LIGNE_ROUGE,
	TRIANGLE_VERT,
	ZIG1_CYAN,
	ZIG2_GRIS,
	L1_ROSE,
	L2_JAUNE,
	TETRIS_RIEN, 
	TETRIS_BLOCKING
} TetrisValeur;


//tableau du jeu 
typedef struct Grille Grille;
struct Grille {
	int s_Tetris[GRILLE_COLUMN_NUMBER][GRILLE_LINE_NUMBER];
};

Grille getNewGrille();
void showGrille(Grille grille);

int getHeight(Grille grille);
int getLines(Grille grille);
int getHoles(Grille grille);
int getBumpiness(Grille grille);
int getNumberBlockedLines(Grille grille);
bool isLine(Grille grille, int line);
int columnHeight(Grille grille, int column);
#endif