#ifndef POINT_H
#define POINT_H
typedef struct Point Point;
struct Point {
	int x;
	int y;
};
Point new_point(int x, int y);
#endif