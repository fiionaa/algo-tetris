#ifndef UTILSAFFICHAGE_H
#define UTILSAFFICHAGE_H

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "Piece.h"

#define DEFAULT_SIZE 32
#define CHAR_WIDTH 18

void initSpritesSurfaces(char* backgroundsPath, char* spritesPath, char* textPath, SDL_Surface* win_surf_base);
void displayText(char* text, const int xOffset, const int yOffset);
void displayInt(int value, const int xOffset, const int yOffset);
void displayPiece(TypePiece type, int xdst, int ydst);

#endif