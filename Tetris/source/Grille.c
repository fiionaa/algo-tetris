#include "Grille.h"

Grille getNewGrille() {
	Grille grille;
	
	for (int i = 0; i < GRILLE_COLUMN_NUMBER; i++) {
		for (int j = 0; j < GRILLE_LINE_NUMBER; j++) {
			grille.s_Tetris[i][j] = TETRIS_RIEN;
		}
	}
	return grille;
}
void showGrille(Grille grille) {
	printf("Affichage grille\n");
	for (int i = 0; i < GRILLE_COLUMN_NUMBER; i++) {
		for (int j = 0; j < GRILLE_LINE_NUMBER; j++) {
			printf("%i ", grille.s_Tetris[i][j]);
		}
		printf("%s\n", " | ");
	}
	printf("%s\n", "---------------------------------");
}

int getHeight(Grille grille) {
	int total = 0;
	for (int c = 0; c < GRILLE_COLUMN_NUMBER; c++) {
		total += columnHeight(grille, c);
	}
	return total;
}

int getLines(Grille grille) {
	int count = 0;
	for (int r = 0; r < GRILLE_LINE_NUMBER; r++) {
		if (isLine(grille, r)) {
			count++;
		}
	}
	return count;
}

int getHoles(Grille grille) {
	int count = 0;
	for (int c = 0; c < GRILLE_COLUMN_NUMBER; c++) {
		bool block = false;
		for (int r = 0; r < GRILLE_LINE_NUMBER; r++) {
			if (grille.s_Tetris[c][r] != 0) {
				block = true;
			}
			else if (grille.s_Tetris[c][r] == 0 && block) {
				count++;
			}
		}
	}
	return count;
}

int getBumpiness(Grille grille) {
	int total = 0;
	for (int c = 0; c < GRILLE_COLUMN_NUMBER - 1; c++) {
		total += abs(columnHeight(grille, c) - columnHeight(grille, c+1));
	}
	return total;
}

int getNumberBlockedLines(Grille grille) {
	int blockingIndex = GRILLE_LINE_NUMBER - 1;
	int blockedLines = 0;
	bool found = false;

	while (!found) {
		if (grille.s_Tetris[0][blockingIndex] != TETRIS_BLOCKING) {
			found = true;
		}
		else {
			blockingIndex--;
			blockedLines++;
		}
	}
	return blockedLines;
}

bool isLine(Grille grille, int line) {
	for (int c = 0; c < GRILLE_COLUMN_NUMBER; c++) {
		if (grille.s_Tetris[c][line] != TETRIS_RIEN && grille.s_Tetris[c][line] != TETRIS_BLOCKING) {
			return true;
		}
	}
	return false;
}

int columnHeight(Grille grille, int column) {
	int r = 0;
	for (r; r < GRILLE_LINE_NUMBER && (grille.s_Tetris[column][r] == TETRIS_RIEN || grille.s_Tetris[column][r] == TETRIS_BLOCKING); r++);
	return GRILLE_LINE_NUMBER - getNumberBlockedLines(grille) - r;
}