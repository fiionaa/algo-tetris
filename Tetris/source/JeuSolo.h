#ifndef JEUSOLO_H
#define JEUSOLO_H

#include <SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include "utilsBoutons.h"
#include "utilsAffichage.h"
#include "Grille.h"
#include "Piece.h"
#include "Scores.h"
#include "Jeu.h"

void initJeuSolo();
void displayTerrainSolo();
void updateGameScreenSolo();
void startJeuSolo(SDL_Surface* win_surf_base, SDL_Window* pWindow_base);

#endif